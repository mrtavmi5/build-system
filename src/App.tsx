import { ThemeProvider } from "styled-components";
import { Flex } from "./elements/Flex";
import { Heading } from "./elements/Heading";
import { Wrapper } from "./elements/Wrapper";
import { theme, invertTheme } from "./theme";


function App() {
  return <ThemeProvider theme={theme}>
    <Heading primary>Lorem ipsum</Heading>
    <Flex justify={"center"}>
      <Heading primary>Consectetur adipiscing elit</Heading>
      <Wrapper>
        <Heading>Consectetur adipiscing elit</Heading>
      </Wrapper>
      <Heading>Consectetur adipiscing elit</Heading>
    </Flex>
    <Flex col bg="blue">
      <Heading primary>Consectetur adipiscing elit</Heading>
      <Heading>Consectetur adipiscing elit</Heading>
      <Heading>Consectetur adipiscing elit</Heading>
    </Flex>
    <Wrapper>
      <Heading primary>HEADER</Heading>

      <ThemeProvider theme={invertTheme}>
        <Heading>HEADER 2</Heading>
        <Flex>
          <Wrapper bg={"yellow"} padding={"0em"} hoverBg="red">
            <div>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat.
            </div>
          </Wrapper>
          <div>
            Duis aute irure dolor in reprehenderit in voluptate velit esse
            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
            cupidatat non proident, sunt in culpa qui officia deserunt mollit
            anim id est laborum.
          </div>
        </Flex>
      </ThemeProvider>
    </Wrapper>
  </ThemeProvider>;
}

export default App;
