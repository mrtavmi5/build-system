import styled from "styled-components";


interface HeadingProps {
  readonly primary?: boolean;
};

export const Heading = styled.h1<HeadingProps>`
  font-size: 1.5em;
  text-align: center;
  font-family: sans-serif;
  color: ${(props) => (props.primary ? "green" : "white")};
  text-align: ${(props) => (props.primary ? "center" : "left")};
`;
