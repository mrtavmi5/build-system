import styled from "styled-components";

interface WrapperProps {
  readonly bg?: string;
  readonly hoverBg?: string;
  readonly padding?: string;
  readonly margin?: string;
};

export const Wrapper = styled.div<WrapperProps>`
  padding: ${(props) => props.padding || "4em"};
  margin: ${(props) => props.margin || "4em"};
  background: ${(props) => props.bg || "red"};

  &:hover {
    background: ${(props) => props.hoverBg || "green"};
  }
`;
