import styled from "styled-components";

interface FlexProps {
  readonly bg?: string;
  readonly justify?: string;
  readonly col?: boolean;
};

export const Flex = styled.div<FlexProps>`
  background-color: ${(props) => props.bg || props.theme.fg};
  display: flex;
  flex-direction: ${(props) => (props.col ? "column" : "row")};
  justify-content: ${(props) => props.justify};
`;
